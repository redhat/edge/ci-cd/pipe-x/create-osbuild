Describe 'clone-repo'
  Include ./lib/clone-repo.sh

  REPO_URL="https://my-org.org/repo.git"
  REPO_DIR="osbuild-manifests"
  BAD_REVISION="fake-branch"
  GOOD_REVISION="d65e6069"

  clean_environment() {
    unset GIT_SSL_NO_VERIFY
    rm -fr "${REPO_DIR}"
  }
  git() {
    if [[ "$1" == "clone" ]]; then
      mkdir -p "${3}/.git/"
      touch "${3}/.git/config"
    elif [[ "$1" == "checkout" ]]; then
      if [[ "$2" == "$GOOD_REVISION" ]]; then
        echo "git checkout ${GOOD_REVISION}"
        return 0
      else
        echo "ERROR: ${2} not found at the git repository" >&2
        return 1
      fi
    fi
    echo "git $*"
  }
  pushd() {
    if [[ ! -d "$1" ]]; then
      echo "pushd: $1: No such file or directory" >&2
      return 1
    fi
    echo "$1 $PWD"
  }
  popd() {
    echo "$PWD"
  }

  Describe 'install dependencies'
    Mock dnf
      echo "dnf $*"
    End

    It 'installs git'
      When call install_git
        The output should include "git"
    End
  End

  Describe 'check_ssl'
    BeforeEach 'unset GIT_SSL_NO_VERIFY'

    It 'disable ssl to git if SSL_VERIFY is false'
      SSL_VERIFY="false"
      When call check_ssl
        The variable GIT_SSL_NO_VERIFY should equal "true"
    End

    It 'do not disable ssl to git if SSL_VERIFY is true'
      SSL_VERIFY="true"
      When call check_ssl
        The variable GIT_SSL_NO_VERIFY should not be defined
    End
  End

  Describe 'git_clone'
    BeforeEach 'clean_environment'

    Context "Use SSL_VERIFY"
      Parameters
        "false" "be defined" "equal true"
        "true" "not be defined" "not be defined"
      End

      It "disable ssl to git if SSL_VERIFY is $1"
        SSL_VERIFY="$1"
        When call git_clone "$REPO_URL" "$REPO_DIR"
          The variable GIT_SSL_NO_VERIFY should $2
          The variable GIT_SSL_NO_VERIFY should $3
          The line 1 of stdout should equal "git clone https://my-org.org/repo.git osbuild-manifests"
      End
    End
  End

  Describe 'git_checkout'
    Context "Revision"
      It 'fails with the wrong revision'
        When call git_checkout "$REPO_DIR" "$BAD_REVISION"
	      The error should include "ERROR: ${BAD_REVISION} not found at the git repository"
        The stdout should include "${REPO_DIR} ${PWD}"
        The status should be failure
      End
      It 'succseed with the right revision'
        When call git_checkout "$REPO_DIR" "$GOOD_REVISION"
	        The error should be blank
          The line 2 of stdout should equal "git checkout $GOOD_REVISION"
          The status should be success
      End
    End
  End

End
