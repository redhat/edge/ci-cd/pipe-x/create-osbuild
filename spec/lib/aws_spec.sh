Describe 'aws'
  Include ./lib/aws.sh

  Describe 'install dependencies'
    Mock dnf
      echo "dnf $*"
    End
    Mock pip3
      echo "pip3 $*"
    End

    It 'installs jq'
      When call install_dependencies
        The output should include "jq"
    End

    It 'installs awscli'
      When call install_dependencies
        The output should include "awscli"
    End
  End

  Describe 'configure_aws_settings'
    REGION="dummy-region"
    setup() {
      touch test-config
      export AWS_CONFIG_FILE="./test-config"
    }
    cleanup() {
      rm -rf ./test-config
    }
    BeforeAll setup
    AfterAll cleanup
    It 'configures the aws region '
      When call configure_aws_settings "${REGION}"
      The file "${AWS_CONFIG_FILE}" should be exist
      The output should be present
      The output should include "region"
      The output should include "dummy-region"
      The output should include "./test-config"
      The contents of file "${AWS_CONFIG_FILE}" should include "region = ${REGION}"
      The contents of file "${AWS_CONFIG_FILE}" should include "output = json"
    End
  End

  Describe 'is_release'
    Parameters
      "FAKE_RELEASE-0.1" 0
      "nightly" 1
    End
    It 'should be return 0 only when is not a nightly'
      RELEASE="$1"

      When call is_release
      The status should eq $2
    End
  End

  Describe 'to_keep'
    Parameters
      "FAKE_RELEASE-0.1" "True"
      "nightly" "False"
    End
    It 'should be return "True" only when is a release'
      RELEASE="$1"

      When call to_keep
      The output should include "$2"
    End
  End

  Describe 'set_build_format'
    It 'set raw format if img'
      BUILD_FORMAT="img"

      When call set_build_format
      The output should equal "raw"
    End

    It 'leaves the same format if it is not img'
      BUILD_FORMAT="qcow2"

      When call set_build_format
      The output should equal "$BUILD_FORMAT"
    End
  End

  Describe 'compress_build_logs'
    Mock tar
      :
    End
    build_logs_dir_name="fake_build_logs_dir"

    It "compresses the logs directory"

      When call compress_build_logs "build_logs_dir" "${build_logs_dir_name}"
      The output should equal "fake_build_logs_dir.tgz"
    End
  End

  Describe 'package_ostree_repo'
    Mock tar
      :
    End
    repo_dir_name="fake_repo_dir"

    It "compresses the image"

      When call package_ostree_repo "images_dir" "${repo_dir_name}"
      The output should equal "fake_repo_dir.tar"
    End
  End

  Describe 'compress_image'
    Mock xz
      :
    End
    IMAGE_FILE="fake_image_name"

    It "compresses the image"

      When call compress_image "images_dir" "${IMAGE_FILE}"
      The output should equal "fake_image_name.xz"
    End
  End

  Describe 'uncompress_image'
    Mock unxz
      :
    End
    IMAGE_FILE="fake_image_name.xz"

    It "uncompresses the image"

      When call uncompress_image "images_dir" "${IMAGE_FILE}"
      The output should equal "fake_image_name"
    End
  End

  Describe 's3_cp'
    aws() {
      :;
    }
    It 'success with 3 argument variables'
      When call s3_cp "src_file" "destination" "aws_vars"
      The length of error should eq 0
      The status should be success
    End

    It 'fails with missing argument'
      ERROR_CODE=5
      When call s3_cp
      The error should include "Expecting 2 or 3 variables defined but got"
      The status should eq $ERROR_CODE
    End
  End

  Describe 'get_compose'
    curl() {
      # Simplified version (with fake info) of the actual composeinfo.json
      cat <<EOF
{
    "header": {
        "type": "productmd.composeinfo",
        "version": "1.2"
    },
    "payload": {
        "compose": {
            "final": false,
            "id": "FAKE-9.3.0-20230317.27",
            "type": "production"
        },
        "release": {
            "internal": false,
            "name": "FAKE Linux Distro",
            "short": "FAKE",
            "version": "9.3.0"
        }
    }
}
EOF
    }

    It 'returns the actual compose URL when the release is nightly'
      DOWNSTREAM_COMPOSE_URL="https://repo.dummy/fake/nightly/fake-9/latest-FAKE/compose"
      RELEASE_NAME="nightly"
      When call get_compose "${DOWNSTREAM_COMPOSE_URL}"
      The output should equal "https://repo.dummy/fake/composes/fake-9/FAKE-9.3.0-20230317.27/compose"
    End

    It 'returns the same compose URL when the release is NOT nightly'
      DOWNSTREAM_COMPOSE_URL="https://repo.dummy/fake/composes/fake-9/FAKE-9.8/compose"
      RELEASE_NAME="MY_RELEASE-0.01"
      When call get_compose "${DOWNSTREAM_COMPOSE_URL}"
      The output should equal "https://repo.dummy/fake/composes/fake-9/FAKE-9.8/compose"
    End
  End

  Describe 'fix_arch_name'
    It 'set arm64 if arch is aarch64'
      ARCH="aarch64"

      When call fix_arch_name $ARCH
      The output should equal "arm64"
    End

    It 'leaves the same arch if it is not aarch64'
      ARCH="x86_64"

      When call fix_arch_name $ARCH
      The output should equal "$ARCH"
    End
  End

  Describe 'set_s3_updload_prefix'
    UUID="XXXX"
    Describe 'use raw-images for imported and sample-images for Sample Images'
      Parameters
      # IMPORT_IMAGE, SAMPLE_IMAGE, images dir
        "True" "True" "raw-images"
        "yes" "Yes" "raw-images"
        "True" "" "raw-images"
        "False" "True" "sample-images"
        "no" "yes" "sample-images"
        "" "True" "sample-images"
        "" "" "non-sample-images"
        "False" "False" "non-sample-images"
      End

      It "IMPORT_IMAGE=$1 SAMPLE_IMAGE=$2"
        IMPORT_IMAGE="$1"
        SAMPLE_IMAGE="$2"
	IMAGE_DIR="$3"
        When call set_s3_upload_prefix
        The output should equal "${UUID}/${IMAGE_DIR}"
      End
    End

    Describe "uses pre-existing S3_UPLOAD_DIR variable"
      Parameters
        "True" "my-fake-images" "my-fake-images"
        "True" "" "sample-images"
        "" "my-fake-images" "my-fake-images"
        "" "" "non-sample-images"
      End

      It "uses SAMPLE_IMAGES='$1' S3_UPLOAD_DIR='$2'"
        SAMPLE_IMAGE="$1"
        S3_UPLOAD_DIR="$2"
        When call set_s3_upload_prefix
        The output should equal "XXXX/${3}"
      End
    End
  End

  Describe 'do_not_import'
    Describe 'return 1 when True/yes and 0 when other values'
      Parameters
        "True" 1
        "yes" 1
        "False" 0
        "no" 0
	"" 0 # variable not defined or empty
      End

      It "IMPORT_IMAGE=$1"
        IMPORT_IMAGE="$1"
        When call do_not_import_image
        The status should equal $2
      End
    End
  End

  Describe 'create_cheksum_file'
    sha256sum () {
      filename="$1"
      if [ -f "${filename}" ]; then
        echo "907ac29fbd1d9f2a76f9e60938f2fbd02f9a752c4f633e326029185bc472856d ${filename}"
        return 0
      else
        echo "sha256sum: ${filename}: No such file or directory"
        return 1
      fi
    }
    IMAGE_NAME="fake-image-qcow2.xz"
    DIR_NAME=$(mktemp -d)
    cleanup_files() { rm -fr "${DIR_NAME}/*"; }
    cleanup() { rm -fr "$DIR_NAME"; }
    Before 'cleanup_files'
    AfterAll 'cleanup'

    It 'generates a checksum valid file'
      touch "${DIR_NAME}/${IMAGE_NAME}"
      When call create_checksum_file "$DIR_NAME" "$IMAGE_NAME"
        The path "${DIR_NAME}/${IMAGE_NAME}.sha256" should be file
        The contents of file "${DIR_NAME}/${IMAGE_NAME}.sha256" should include "907ac29fbd1d9f2a76f9e60938f2fbd02f9a752c4f633e326029185bc472856d $IMAGE_NAME"
    End

    It 'fails if the file does not exist'
      When call create_checksum_file "$DIR_NAME" "fake_file"
        The path "${DIR_NAME}/fake_file.sha256" should be file
        The contents of file "${DIR_NAME}/fake_file.sha256" should include "No such file or directory"
        The status should equal 1
    End
  End

  Describe 'import_snapshot'
    aws() {
      BUCKET="${2%%.*}"
      KEY="${3%%.*}"
      cat <<EOF
{
    "Description": "My server VM",
    "ImportTaskId": "import-snap-1234567890abcdef0",
    "SnapshotTaskDetail": {
        "Description": "My server VMDK",
        "DiskImageSize": "0.0",
        "Format": "VMDK",
        "Progress": "3",
        "Status": "active",
        "StatusMessage": "pending",
        "UserBucket": {
            "S3Bucket": "${BUCKET}",
            "S3Key": "${KEY}"
        }
    }
}
EOF
    }
    It 'returns a json when passed valid s3 bucket and s3 key arguments'
        When call import_snapshot "s3-bucket" "s3-key"
        The error should be blank
        The output should include "import-snap-1234567890abcdef0"
    End
    Describe 'failure with missing parameter'
      Parameters
        "" "s3-key"
        "s3-bucket" ""
      End
      It 'fails with missing parameter'
        ERROR_CODE=5
        When call import_snapshot $1 $2
        The error should include "Expecting 2 variables defined but got"
        The status should eq 5
      End
    End
  End

  Describe 'snapshots'
    Mock aws
      # Expected command:
      # aws ec2 describe-import-snapshot-tasks --import-task-ids "${import_task_id}"
      if [[ "$4" == "import-snap-000000" ]]; then
        cat <<EOF
{
  "ImportSnapshotTasks": [
    {
      "ImportTaskId": "import-snap-000000",
      "SnapshotTaskDetail": {
        "DiskImageSize": 8589934592.0,
        "Format": "RAW",
        "SnapshotId": "snap-000000",
        "Status": "completed",
        "UserBucket": {
          "S3Bucket": "auto-toolchain-ci-gitlab-workspaces",
          "S3Key": "fake_image.raw"
        }
      },
      "Tags": []
    }
  ]
}
EOF
      else
        echo "An error occurred (InvalidConversionTaskId.Malformed) when calling the DescribeImportSnapshotTasks operation: Tasks $4 not found"
      fi
    End
    IMPORT_TASK_ID="import-snap-000000"

    Describe 'describe_import_snapshot_tasks'
      It 'returns a json when passed a valid ImportTaskId'
        When call describe_import_snapshot_tasks "$IMPORT_TASK_ID"
        The output should include '"ImportTaskId": "import-snap-000000"'
      End
      It 'fail when passed a non valid ImportTaskId'
        When call describe_import_snapshot_tasks "import-snap-fake"
        The output should include "Tasks import-snap-fake not found"
        # It should fail but the function doesn't implement it
        #The status should be failure
      End
    End

    Describe 'import_status'
      It 'returns valid status when passed a valid ImportTaskId'
        When call import_status "$IMPORT_TASK_ID"
        The output should include 'completed'
      End
      It 'fail when passed a non valid ImportTaskId'
        When call import_status "import-snap-fake"
        The status should be failure
        The error should include "Invalid numeric literal"
      End
    End

    Describe 'snapshot_id'
      It 'returns valid SnapshotId when passed a valid ImportTaskId'
        When call snapshot_id "$IMPORT_TASK_ID"
        The output should include 'snap-000000'
      End
      It 'fail when passed a non valid ImportTaskId'
        When call snapshot_id "import-snap-fake"
        The status should be failure
        The error should include "Invalid numeric literal"
      End
    End

    Describe 'create_snapshot_tags'
      aws() {
        return 0
      }

      It 'success with correct tag arguments'
        When call create_snapshot_tags "snap-000000" "fake_region"
        The output should be blank
        The error should be blank
      End

      It 'fails with missing one argument'
        ERROR_CODE=5
        When call create_snapshot_tags "snap-000000"
        The error should include "Expecting 2 variable defined but got"
        The status should eq $ERROR_CODE
      End

      It 'fails with missing all arguments'
        ERROR_CODE=5
        When call create_snapshot_tags
        The error should include "Expecting 2 variable defined but got"
        The status should eq $ERROR_CODE
      End
    End

    Describe 'copy_snapshot'
      aws() {
        cat << EOF
{
    "SnapshotId": "snap-0000"
}
EOF
      }
      It 'copy snapshot with given id from source to destination'
        When call copy_snapshot "snap-0000" "src" "dest"
        The output should include "snap-0000"
        The error should be blank
      End

      It 'fails with missing parameters'
        ERROR_CODE=5
        When call copy_snapshot "src" "dest"
        The error should include "Expecting 3 variables defined but got"
        The status should eq $ERROR_CODE
      End
    End

    Describe 'snapshot_state'
      aws() {
        cat <<EOF
{
    "Snapshots": [
        {
            "Description": "This is my snapshot",
            "Encrypted": false,
            "VolumeId": "vol-049df61146c4d7901",
            "State": "completed",
            "VolumeSize": 8,
            "StartTime": "2019-02-28T21:28:32.000Z",
            "Progress": "100%",
            "OwnerId": "012345678910",
            "SnapshotId": "snap-0000",
            "Tags": [
                {
                    "Key": "Stack",
                    "Value": "test"
                }
            ]
        }
    ]
}
EOF
      }
      It 'returns snapshot state with given snapshot id and region'
        When call snapshot_state "snap-0000:us-east-0"
        The error should be blank
        The output should eq "completed"
      End

      It 'fails with missing snapshot id'
        ERROR_CODE=5
        When call snapshot_state
        The error should include "Invalid argument, please use <snapshot_id>:<region> formant"
        The status should eq $ERROR_CODE
      End
    End

    Describe 'register_image'
      aws() {
        cat <<EOF
{
    "ImageId": "ami-1a2b3c4d5eEXAMPLE"
}
EOF
      }

      It 'returns ImageId in json format when the image is succefully registered'
        When call register_image "name" "region" "arch" "root_device" "boot_mode" "snapshot_id"
        The output should eq "ami-1a2b3c4d5eEXAMPLE"
        The error should be blank
      End

      It 'fails with missing arguments'
        ERROR_CODE=5
        When call register_image
        The error should include "Expecting 6 variables defined but got"
        The status should eq $ERROR_CODE
      End
    End

    Describe 'create_image_tags'
      aws() {
        :;
      }

      It 'success with the correct input arguments'
        When call create_image_tags "id" "region" "uuid"
        The error should be blank
        The output should be blank
      End

      It 'fails with missing arguments'
        ERROR_CODE=5
        When call create_image_tags
        The error should include "Expecting at least 3 variables"
        The status should eq $ERROR_CODE
      End
    End

    Describe 'deregister_image'
      aws() {
        :;
      }
      # We assume there is a AMI to redegister
      It 'success and no message if an ami_id is passed'
        When call deregister_image "image_id" "region"
        The error should be blank
        The output should be blank
        The status should be success
      End
      It 'success but message about no AMI found if no ami_id is passed'
        When call deregister_image "" "region"
        The error should equal "No public AMI found"
        The output should be blank
        The status should be success
      End
    End

    Describe 'grant_image_permissions'
      aws() {
        :;
      }
      deregister_image() {
        :;
      }
      It 'success with correct arguments'
        When call grant_image_permissions "image" "region" "user-id"
        The error should be blank
        The output should be blank
        The status should be success
      End
      It 'fails with missing arguments'
        ERROR_CODE=5
        When call grant_image_permissions
        The error should include "Expecting 3 variables defined but got"
        The status should eq $ERROR_CODE
      End
      It 'deregister old AMI successfuly when user_id is all'
        find_oldest_public_image() {
          echo "ami-aa00bb11cc22"
        }
        When call grant_image_permissions "image" "region" "all"
        The error should be blank
        The output should be blank
        The status should be success
      End
      It 'successfuly when user_id is all, but there is no public AMI'
        find_oldest_public_image() {
          :;
        }
        When call grant_image_permissions "image" "region" "all"
        The error should be blank
        The output should be blank
        The status should be success
      End
    End
  End

  Describe 'register-images'
   Mock aws
    cat <<EOF
{
    "Images": [
        {
            "VirtualizationType": "hvm",
            "Description": "Provided by Red Hat, Inc.",
            "PlatformDetails": "Red Hat Enterprise Linux",
            "EnaSupport": true,
            "Hypervisor": "xen",
            "State": "available",
            "SriovNetSupport": "simple",
            "ImageId": "ami-1234567890EXAMPLE",
            "UsageOperation": "RunInstances:0010",
            "BlockDeviceMappings": [
                {
                    "DeviceName": "/dev/sda1",
                    "Ebs": {
                        "SnapshotId": "snap-111222333444aaabb",
                        "DeleteOnTermination": true,
                        "VolumeType": "gp2",
                        "VolumeSize": 10,
                        "Encrypted": false
                    }
                }
            ],
            "Architecture": "x86_64",
            "ImageLocation": "123456789012/RHEL-8.0.0_HVM-20190618-x86_64-1-Hourly2-GP2",
            "RootDeviceType": "ebs",
            "OwnerId": "123456789012",
            "RootDeviceName": "/dev/sda1",
            "CreationDate": "2019-05-10T13:17:12.000Z",
            "Public": true,
            "ImageType": "machine",
            "Name": "RHEL-8.0.0_HVM-20190618-x86_64-1-Hourly2-GP2"
        }
    ]
}
EOF
    End
    IMAGE_ID="ami-1234567890EXAMPLE"

    Describe 'image_register_info'
      It 'returns Images json for requested image id'
        When call image_register_info "${IMAGE_ID}" "dummy-region"
        The output should include "\"ImageId\": \"${IMAGE_ID}\","
        The error should be blank
      End

      It 'fails with missing argument variables'
        ERROR_CODE=5
        When call image_register_info
        The error should include "Expecting 2 variables defined but got"
        The status should eq $ERROR_CODE
      End
    End

    Describe 'image_register_status'
      It 'returns the image state with given image_id:region'
        When call image_register_status "image:dummy-region"
        The error should be blank
        The output should eq "available"
      End

      It 'fails with wrong argument'
        ERROR_CODE=5
        When call image_register_status "wrong-argument"
        The error should include "Invalid argument"
        The status should eq $ERROR_CODE
      End
    End

    Describe 'ami_registered_from_snapshot'
      It 'returns Image ID for an a requested image IMAGE_NAME'
        When call ami_registered_from_snapshot "${IMAGE_NAME}" "dummy-region"
        The output should include "${IMAGE_ID}"
        The error should be blank
      End

      It 'fails with missing argument variables'
        ERROR_CODE=5
        When call ami_registered_from_snapshot
        The error should include "Expecting 2 variables defined but got"
        The status should eq $ERROR_CODE
      End
    End
  End

  Describe 'using ami_registered_from_snapshot to check image that is not register with AMI'
    aws() {
      echo "{}"
    }
    It 'returns null for a requested IMAGE_NAME'
      When call ami_registered_from_snapshot "${IMAGE_NAME}" "dummy-region"
      The output should eq "null"
      The error should be blank
    End
  End

  Describe 'task_progress'
    It 'fails with missing arguments'
      ERROR_CODE=5
      When call task_progress "echo" "dummy-id"
      The error should include 'Expecting 3 or 4 variables defined'
      The status should eq $ERROR_CODE
    End
  End

  Describe 'delete_raw_images'
    BUCKET="my-bucket"
    UPLOAD_PREFIX="prefix"
    IMAGE_KEY="key"
    # Expected command: aws s3 rm "s3://bucket/prefix/file.extension"
    aws() {
      URI="${3}"
      echo "delete: ${URI}"
    }

    Context "remove all the rigth files"
      Parameters
        "raw"
        "raw.sha256"
        "json"
      End

      It "removes the image's $1 file from the s3 bucket"
        When call delete_raw_images "${BUCKET}" "${UPLOAD_PREFIX}" "${IMAGE_KEY}"
        The output should include "delete: s3://${BUCKET}/${UPLOAD_PREFIX}/${IMAGE_KEY}.${1}"
        The error should be blank
      End
    End

    It 'fails with missing arguments'
      ERROR_CODE=5
      When call delete_raw_images
      The error should include "Expecting 3 variables defined but got"
      The status should eq $ERROR_CODE
    End
  End
End
