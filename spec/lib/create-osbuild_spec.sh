Describe 'create-osbuild'
  Include ./lib/create-osbuild.sh
  SSH_KEY="ssh-rsa my-public-key"
  CI_REPOS_ENDPOINT="http://my.fake.repo"
  UUID="XXX"
  ARCH="ppc64"
  DISTRO="cs9"
  STREAM="upstream"
  COMPOSE_DISTRO="AutoSD"
  AIB="automotive-image-builder"
  TMT_TREE="$PWD"

  _get_image_manifest() {
    echo "images/minimal.ipp.yml"
  }
  Mock automotive-image-builder
    echo "$*"
  End

  Describe 'set variables'
    OS_PREFIX="cs"
    OS_VERSION="9"
    BUILD_TARGET="qemu"
    IMAGE_NAME="minimal"
    IMAGE_TYPE="ostree"
    ARCH="aarch64"
    BUILD_FORMAT="img"
    PACKAGE_SET="autosd9"

    It 'sets the proper DISTRO variable'
      When call set_vars
      The value "${DISTRO}" should eq "autosd9"
      The value "${DISTRO}" should not eq "cs8"
      The value "${DISTRO}" should not eq "rhel9"
    End

    It 'sets the proper DISK_IMAGE'
      When call set_vars
      The value "${DISK_IMAGE}" should eq "autosd9-qemu-minimal-ostree.aarch64.img"
    End

    It 'sets the proper format for raw images'
      When call set_vars
      The value "${DISK_IMAGE}" should end with ".img"
      The value "${IMAGE_FILE}" should end with ".raw"
      The value "${IMAGE_FILE}" should not end with ".qcow2"
    End

    It 'sets the proper format for qcow2 images'
      BUILD_FORMAT=qcow2
      When call set_vars
      The value "${DISK_IMAGE}" should end with ".qcow2"
      The value "${IMAGE_FILE}" should end with ".qcow2"
      The value "${IMAGE_FILE}" should not end with ".raw"
    End

  End

  Describe 'add UEFI vendor'
    UEFI_VENDOR="fake_vendor"

    It 'adds the param uefi_vendor'
      When call add_uefi_vendor
      The value "${OS_OPTIONS[*]}" should include "uefi_vendor: fake_vendor"
    End
  End

  Describe 'add OSTree OS name'
    OSTREE_OS_NAME="distroX"

    It 'adds the param uefi_vendor'
      When call add_ostree_os_name
      The value "${OS_OPTIONS[*]}" should include "osname: distroX"
    End
  End

  Describe 'enable SSH'
    It 'adds ssh packages and enable ssh in the image'
      When call enable_ssh
      The value "${OS_OPTIONS[*]}" should include "ssh_permit_root_login: true"
      The value "${OS_OPTIONS[*]}" should include "extra_rpms: [openssh-server,python3,polkit,rsync]"
      The value "${OS_OPTIONS[*]}" should include "ssh_permit_password_auth: false"
      The value "${OS_OPTIONS[*]}" should not include "ssh_permit_password_auth: true"
    End
  End

  Describe 'enable SSH password auth'
    ENABLE_SSH_PASSWORD_AUTH="true"

    It 'enable ssh password auth in the image'
      When call enable_ssh
      The value "${OS_OPTIONS[*]}" should include "ssh_permit_password_auth: true"
      The value "${OS_OPTIONS[*]}" should not include "ssh_permit_password_auth: false"
    End
  End

  Describe 'disable module sig enforce'
    USE_MODULE_SIG_ENFORCE="false"

    It 'disable module sig enforce'
      When call disable_module_sig_enforce
      The value "${OS_OPTIONS[*]}" should include "use_module_sig_enforce: false"
    End
  End

  Describe 'add SSH key'
    It 'adds specific public ssh key'
      When call add_ssh_key "$SSH_KEY"
      The value "${OS_OPTIONS[*]}" should include "root_ssh_key: 'ssh-rsa my-public-key'"
    End
  End

  Describe 'set image size'
    It 'sets a smaller default image size'
      When call set_vars
      The value "${IMAGE_SIZE}" should eq "4294967296"
    End

    It 'passes a value for image size'
      When call set_image_size 4000000000
      The value "${OS_OPTIONS[*]}" should include "image_size: '4000000000'"
    End
  End

  Describe 'override kernel rpm'
    override_yum_repo_with_product_build() { :; }
    PACKAGE_SET=cs9
    PRODUCT_BUILD_PREFIX=autosd9

    It 'passes a variable tooverride the kernel package'
      KERNEL_RPM="kernel-automotive-debug"
      When call override_kernel_rpm "${KERNEL_RPM}"
      The value "${OS_OPTIONS[*]}" should include "kernel_package: kernel-automotive-debug"
    End

    It 'passes a variable tooverride the kernel package when call set_osbuild_options'
      KERNEL_RPM="kernel-automotive-debug"
      When call set_osbuild_options
      The value "${OS_OPTIONS[*]}" should include "kernel_package: kernel-automotive-debug"
    End
  End

  Describe 'check extra_kernel_opts variable'
    override_yum_repo_with_product_build() { :; }
    It 'passes userdata variable to extra_kernel_opts when BUILD_TARGET is qdrive3'
      BUILD_TARGET="qdrive3"
      When call add_aboot_kernel_opts
      The value "${OS_OPTIONS[*]}" should include "PARTLABEL=userdata"
    End
    It 'passes system_a to extra_kernel_opts when BUILD_TARGET is not qdrive3'
      BUILD_TARGET="FAKE-TARGET"
      When call add_aboot_kernel_opts
      The value "${OS_OPTIONS[*]}" should include "PARTLABEL=system_a"
    End
  End

  Describe 'add info for the build-info file'
    override_yum_repo_with_product_build() { :; }
    It 'adds the UUID'
      UUID="F000000000-ID"
      When call add_build_info
      The value "${OS_OPTIONS[*]}" should include "image_uuid: F000000000-ID"
    End
    It 'adds the RELEASE'
      RELEASE="FAKE-1.0"
      When call add_build_info
      The value "${OS_OPTIONS[*]}" should include "release_name: FAKE-1.0"
    End
  End

  Describe 'set_osbuild_options'
    override_yum_repo_with_product_build() { :; }
    It 'should set the uefi_vendor'
      UEFI_VENDOR="fake_vendor"

      When call set_osbuild_options
      The status should be success
      The value "${OS_OPTIONS[*]}" should include "uefi_vendor: fake_vendor"
    End

    It 'should set the osname for OSTree images'
      IMAGE_TYPE="ostree"
      OSTREE_OS_NAME="distroX"

      When call set_osbuild_options
      The status should be success
      The value "${OS_OPTIONS[*]}" should include "osname: distroX"
    End

    It 'should support bool formats yes'
      TEST_IMAGE="yes"
      PACKAGE_SET="cs9"
      PRODUCT_BUILD_PREFIX="autosd9"

      When call set_osbuild_options
      The status should be success
      The value "${OS_OPTIONS[*]}" should include "ssh_permit_root_login: true"
    End

    It 'should support bool formats no'
      TEST_IMAGE="no"
      PACKAGE_SET="cs9"
      PRODUCT_BUILD_PREFIX="autosd9"

      When call set_osbuild_options
      The status should be success
      The value "${OS_OPTIONS[*]}" should not include "ssh_permit_root_login: true"
    End

    Describe 'set options for building an Upstream image with no Product build and not for testing'
      TEST_IMAGE="no"
      PACKAGE_SET="cs9"
      PRODUCT_BUILD_PREFIX="autosd9"

      It 'should no have the OS_OPTIONS for testing'
        When call set_osbuild_options
        The value "${OS_OPTIONS[*]}" should not include "ssh_permit_root_login: true"
        The value "${OS_OPTIONS[*]}" should not include "extra_rpms: [openssh-server,python3,polkit]"
      End

      It 'should not include any ssh key'
        When call set_osbuild_options
        The value "${OS_OPTIONS[*]}" should not include "root_ssh_key:"
      End

      It 'add extra_kernel_opts variable'
        BUILD_FORMAT="aboot"
        When call set_osbuild_options
        The value "${OS_OPTIONS[*]}" should include "extra_kernel_opts"
      End
    End

    Describe 'set options for building an Upstream image with no Product build but for testing'
      TEST_IMAGE=yes
      PACKAGE_SET=cs99
      PRODUCT_BUILD_PREFIX=autosd9

      It 'should no have the OS_OPTIONS for testing'
        When call set_osbuild_options
        The value "${OS_OPTIONS[*]}" should include "ssh_permit_root_login: true"
        The value "${OS_OPTIONS[*]}" should include "extra_rpms: [openssh-server,python3,polkit,rsync]"
      End

      It 'should not include any ssh key'
        When call set_osbuild_options
        The value "${OS_OPTIONS[*]}" should include "root_ssh_key: 'ssh-rsa my-public-key'"
      End
    End

    Describe 'set options for building an Upstream image with Product build but not for testing'
      TEST_IMAGE=no
      PACKAGE_SET=autosd9
      PRODUCT_BUILD_PREFIX=autosd9

      It 'should no have the OS_OPTIONS for testing'
        When call set_osbuild_options
        The value "${OS_OPTIONS[*]}" should not include "ssh_permit_root_login: true"
        The value "${OS_OPTIONS[*]}" should not include "extra_rpms: [openssh-server,python3,polkit,rsync]"
      End

      It 'should not include any ssh key'
        When call set_osbuild_options
        The value "${OS_OPTIONS[*]}" should not include "root_ssh_key:"
      End
    End

    Describe 'set options for building an Upstream image with Product build and for testing'
      TEST_IMAGE=yes
      PACKAGE_SET=autosd9
      PRODUCT_BUILD_PREFIX=autosd9

      It 'should no have the OS_OPTIONS for testing'
        When call set_osbuild_options
        The value "${OS_OPTIONS[*]}" should include "ssh_permit_root_login: true"
        The value "${OS_OPTIONS[*]}" should include "extra_rpms: [openssh-server,python3,polkit,rsync]"
      End

      It 'should not include any ssh key'
        When call set_osbuild_options
        The value "${OS_OPTIONS[*]}" should include "root_ssh_key: 'ssh-rsa my-public-key'"
      End
    End

    Describe 'set options for specific IMAGE_SIZE if SET_IMAGE_SIZE is set'
      Parameters
        "True" "" "image_size: '4294967296'"
        "yes" "8888888888" "image_size: '8888888888'"
        "False" "" ""
        "no" "8888888888" ""
      End

      It "has defined SET_IMAGE_SIZE as \"$1\" and IMAGE_SIZE as \"$2\""
        SET_IMAGE_SIZE="$1"
        IMAGE_SIZE="$2"
        PACKAGE_SET="cs9"
        set_vars
        When call set_osbuild_options
        The value "${OS_OPTIONS[*]}" should include "$3"
      End
    End
  End

  Describe 'build_target'
    Mock make
      echo "make $*"
    End
    setenforce() { :; }
    _save_config_files() { :; }
    override_yum_repo_with_product_build() { :; }
    TMT_PLAN_DATA=$(mktemp -d)
    DEFINES_VAR_FILE=$(mktemp)
    cleanup_temp() {
      rm -fr "${TMT_PLAN_DATA}/*"
      rm -f "${DEFINES_VAR_FILE}"
    }
    BeforeEach 'cleanup_temp'
    AfterEach 'cleanup_temp'

    Describe 'build sample image (not for testing or for rpi4)'
      SAMPLE_IMAGE=yes
      TEST_IMAGE=no
      PACKAGE_SET=autosd9
      PRODUCT_BUILD_PREFIX=autosd9
      COMPOSE_DISTRO="AutoSD"
      set_vars
      set_osbuild_options

      It 'has no the ssh enabled'
        When call build_target
        The output should not include "ssh_permit_root_login: true"
        The output should not include 'extra_rpms: [openssh-server,python3,polkit,rsync]'
      End

      It 'has not the Testing Farm public ssh key'
        When call build_target
        The output should not include '-D root_ssh_key=\"ssh-rsa my-public-key\"'
      End
    End

    Describe 'build sample image (for rpi4)'
      SAMPLE_IMAGE=yes
      TEST_IMAGE=no
      PACKAGE_SET=autosd9
      PRODUCT_BUILD_PREFIX=autosd9
      COMPOSE_DISTRO="AutoSD"
      set_vars
      set_osbuild_options

      It 'has no the ssh enabled'
        When call build_target
        The output should not include "ssh_permit_root_login: true"
        The output should not include 'extra_rpms: [openssh-server,python3,polkit,rsync]'
      End

      It 'has not the Testing Farm public ssh key'
        When call build_target
        The output should not include '-D root_ssh_key="ssh-rsa my-public-key"'
      End
    End

    Describe 'build sample image (for testing)'
      SAMPLE_IMAGE=yes
      TEST_IMAGE=yes
      PACKAGE_SET=autosd9
      PRODUCT_BUILD_PREFIX=autosd9
      COMPOSE_DISTRO="AutoSD"
      set_vars
      set_osbuild_options

      It 'has the ssh enabled'
        When call build_target
        The output should include "ssh_permit_root_login: true"
        The output should include 'extra_rpms: [openssh-server,python3,polkit,rsync]'
      End

      It 'has the Testing Farm public ssh key'
        When call build_target
        The output should include "root_ssh_key: 'ssh-rsa my-public-key'"
      End

      It 'should not use the wrong ssh key'
        When call build_target
        The output should not include "root_ssh_key: 'ssh-rsa my-wrong-key'"
      End
    End

    Describe 'build non sample image (not for testing)'
      SAMPLE_IMAGE=False
      TEST_IMAGE=no
      PACKAGE_SET=cs9
      PRODUCT_BUILD_PREFIX=autosd9
      set_osbuild_options

      It 'has no the ssh enabled'
        When call build_target
        The output should not include "ssh_permit_root_login: true"
        The output should not include 'extra_rpms: [openssh-server,python3,polkit,rsync]'
      End

      It 'has not the Testing Farm public ssh key'
        When call build_target
        The output should not include "root_ssh_key: 'ssh-rsa my-public-key'"
      End
    End

    Describe 'build non sample image (for testing)'
      SAMPLE_IMAGE=False
      TEST_IMAGE=yes
      PACKAGE_SET=cs9
      PRODUCT_BUILD_PREFIX=autosd9
      set_osbuild_options

      It 'has the ssh enabled'
        When call build_target
        The output should include "ssh_permit_root_login: true"
        The output should include 'extra_rpms: [openssh-server,python3,polkit,rsync]'
      End

      It 'adds the Testing Farm public ssh key'
        When call build_target
        The output should include "root_ssh_key: 'ssh-rsa my-public-key'"
      End

      It 'should not use the wrong ssh key'
        When call build_target
        The output should not include "root_ssh_key: 'ssh-rsa my-wrong-key'"
      End
    End

    Describe 'build the QA image is a sample image for testing, but uses CS9'
      SAMPLE_IMAGE=yes
      TEST_IMAGE=yes
      PACKAGE_SET=cs9
      PRODUCT_BUILD_PREFIX=autosd9
      set_osbuild_options

      It 'has the ssh enabled'
        When call build_target
        The output should include "ssh_permit_root_login: true"
        The output should include 'extra_rpms: [openssh-server,python3,polkit,rsync]'
      End

      It 'adds the Testing Farm public ssh key'
        When call build_target
        The output should include "root_ssh_key: 'ssh-rsa my-public-key'"
      End

      It 'should not use the wrong ssh key'
        When call build_target
        The output should not include "root_ssh_key: 'ssh-rsa my-wrong-key'"
      End
    End
  End

  Describe 'create_core_rpms'
    cd() { :; }
    set_vars

    It 'should include the right file'
      DISTRO="autosd"
      ARCH="aarch64"
      RPMS_FILE="autosd-core-rpms-aarch64.txt"
      GENERATE_CORE_RPMS="generate-core-rpms"
      YUM_URL="${BASE_URL}/repos/${COMPOSE_DISTRO}/compose"

      Mock "${GENERATE_CORE_RPMS}"
        distro="$1"
        arch="$2"
        touch "${distro}-core-rpms-${arch}.txt"
      End

      When call create_core_rpms "${DISTRO}" "${ARCH}"
      The value "${BUILD_CMD[*]}" should include "${GENERATE_CORE_RPMS} ${DISTRO} ${ARCH} ${YUM_URL}"
      The file "${RPMS_FILE}" should be exist
    End
  End

  Describe 'save_files'
    TMT_PLAN_DATA=$(mktemp -d)
    DISTRO="fake9"
    BASE_TARGET="fake9-qemu-developer-regular.aarch64"
    MANIFEST="${BASE_TARGET}.json"
    create_manifest() { touch "${MANIFEST}" ; }
    cleanup_temp() { rm -fr "${TMT_PLAN_DATA}/*" "${MANIFEST}" ; }
    BeforeEach 'create_manifest'
    AfterEach 'cleanup_temp'

    It 'copies the osbuild manifest to TMT_PLAN_DATA'
      When call save_files
      The output should include "fake9-qemu-developer-regular.aarch64.json"
      The file "${TMT_PLAN_DATA}/fake9-qemu-developer-regular.aarch64.json" should be exist
    End
  End

End
