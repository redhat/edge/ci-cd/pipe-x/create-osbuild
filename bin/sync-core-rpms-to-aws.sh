#!/bin/bash
set -xeo pipefail

source lib/aws.sh

echo "[+] Install dependencies"
install_dependencies

echo "[+] Configure AWS settings"
configure_aws_settings "${AWS_REGION}"

S3_UPLOAD_PREFIX=$(set_s3_upload_prefix)
echo "[+] Uploading core rpms list to 's3://${S3_BUCKET_NAME}/${S3_KEY}'"
s3_cp "${DOWNLOAD_DIRECTORY}/${PRODUCT_BUILD_PREFIX//[0-9]/}-core-rpms-${ARCH}.txt" \
      "${S3_BUCKET_NAME}/${S3_UPLOAD_PREFIX}/"