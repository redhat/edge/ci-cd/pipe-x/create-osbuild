#!/bin/bash

set -euxo pipefail

source lib/create-osbuild.sh
source lib/build-env.sh

cd "${SAMPLE_IMAGES}" || exit 1

# The sample-images tools/generate-core-rpms script can generate the list of core RPMs
# by setting distro and arch, e.g. "tools/generate-core-rpms cs9 x86_64" generates a
# list of rpms for x86_64 architecture of cs9 distro
set_vars

install_dependencies

echo "[+] Prepare the dev environment"
prepare_env

create_core_rpms "${DISTRO}" "${ARCH}"

echo "[+] Moving the generated core rpms list"
mkdir -p "${DOWNLOAD_DIRECTORY}"
mv "${DISTRO}-core-rpms-${ARCH}.txt" \
   "${DOWNLOAD_DIRECTORY}/${DISTRO//[0-9]/}-core-rpms-${ARCH}.txt"
