#!/bin/bash

set -euxo pipefail

source lib/clone-repo.sh

SAMPLE_IMAGES_REPO="${SAMPLE_IMAGES_REPO:-https://gitlab.com/CentOS/automotive/sample-images.git}"
SAMPLE_IMAGES_REF="${SAMPLE_IMAGES_REF:-main}"
OSBUILD_DIR="${OSBUILD_DIR:-sample-images}"
CUSTOM_IMAGES_REPO="${CUSTOM_IMAGES_REPO:-https://gitlab.com/redhat/edge/ci-cd/pipe-x/custom-images.git}"
CUSTOM_IMAGES_DIR="${OSBUILD_DIR}/custom-images"
CUSTOM_IMAGES_REF="${CUSTOM_IMAGES_REF:-main}"
AIB_REPO="${AIB_REPO:-https://gitlab.com/CentOS/automotive/src/automotive-image-builder.git}"
AIB_REF="${AIB_REF:-main}"
AIB_DIR="${OSBUILD_DIR}/automotive-image-builder"

echo "[+] Install Git"
install_git

echo "[+] Remove any previous mpp files"
rm -vfr "${OSBUILD_DIR}"

echo "[+] Clone the repository with the mpp files"
clone_repo "$SAMPLE_IMAGES_REPO" "$OSBUILD_DIR" "$SAMPLE_IMAGES_REF"
echo "[+] Cloned"

echo "[+] Clone the automotive-image-builder repository"
clone_repo "$AIB_REPO" "$AIB_DIR" "$AIB_REF"
echo "[+] Cloned"

echo "[+] Clone the repository with the custom images mpp files"
clone_repo "$CUSTOM_IMAGES_REPO" "$CUSTOM_IMAGES_DIR" "$CUSTOM_IMAGES_REF"
echo "[+] Cloned"
