#!/bin/bash
set -xeo pipefail

source lib/aws.sh

echo "[+] Install dependencies"
install_dependencies

echo "[+] Configure AWS settings"
configure_aws_settings "${AWS_REGION}"

BUILD_FORMAT=$(set_build_format)

IMAGE_FILE="${IMAGE_KEY}.${BUILD_FORMAT}"
if [ ! -r "${DOWNLOAD_DIRECTORY}/${IMAGE_FILE}" ]; then
  echo "Error: the file ${IMAGE_FILE} doesn't exist"
  exit 1
fi

S3_UPLOAD_PREFIX=$(set_s3_upload_prefix)
s3_upload_target="${S3_BUCKET_NAME}/${S3_UPLOAD_PREFIX}"
s3_path="${S3_BUCKET_NAME}/${S3_KEY}"
IMAGE_OSTREE_REPO=${IMAGE_OSTREE_REPO:-"${IMAGE_KEY}.repo"}
NIGHTLY="${NIGHTLY:-"False"}"
STREAM="${STREAM:-"upstream"}"

if [ -d "${DOWNLOAD_DIRECTORY}/${IMAGE_FILE}" ]; then
  echo "[+] ${IMAGE_FILE} is a directory. Processing files:"
  for filename in "${DOWNLOAD_DIRECTORY}/${IMAGE_FILE}"/*; do
    filename="$(basename "${filename}")"
    echo "[+] Compressing image ${filename}"
    filename=$(compress_image "${DOWNLOAD_DIRECTORY}/${IMAGE_FILE}" "${filename}")
    create_checksum_file "${DOWNLOAD_DIRECTORY}/${IMAGE_FILE}" "${filename}"
    echo "[+] Uploading compressed image ${filename} and its checksum to 's3://${s3_path}'"
    upload_image_and_checksum_to_s3 \
      "${DOWNLOAD_DIRECTORY}/${IMAGE_FILE}/${filename}" "${s3_upload_target}/${IMAGE_FILE}/${filename}"
  done
else
    echo "[+] Compressing image ${IMAGE_FILE}"
    IMAGE_FILE=$(compress_image "${DOWNLOAD_DIRECTORY}" "${IMAGE_FILE}")
    create_checksum_file "${DOWNLOAD_DIRECTORY}" "${IMAGE_FILE}"
    echo "[+] Uploading compressed image ${IMAGE_FILE} and its checksum to 's3://${s3_path}'"
    upload_image_and_checksum_to_s3 "${DOWNLOAD_DIRECTORY}/${IMAGE_FILE}" "${s3_upload_target}/${IMAGE_FILE}"
fi

# If the image has a OSTree repo, create a tarball with it and upload it to S3
if [[ "${OSTREE_REPO}" == "yes" ]] && [[ -d "${DOWNLOAD_DIRECTORY}/${IMAGE_OSTREE_REPO}" ]] ; then
    # Define "local" s3_upload_target and prefix to avoid messing with other uploads
    _s3_upload_prefix=$(set_s3_upload_prefix "${OSTREE_REPO}")
    _s3_upload_target="${S3_BUCKET_NAME}/${_s3_upload_prefix}"
    echo "[+] Packaging the OSTree repo ${IMAGE_OSTREE_REPO}"
    IMAGE_OSTREE_REPO_FILE=$(package_ostree_repo "${DOWNLOAD_DIRECTORY}" "${IMAGE_OSTREE_REPO}")
    create_checksum_file "${DOWNLOAD_DIRECTORY}" "${IMAGE_OSTREE_REPO_FILE}"
    echo "[+] Uploading packaged OSTree repo ${IMAGE_OSTREE_REPO_FILE} and its checksum to 's3://${s3_path}'"
    upload_image_and_checksum_to_s3 "${DOWNLOAD_DIRECTORY}/${IMAGE_OSTREE_REPO_FILE}" "${_s3_upload_target}/${IMAGE_OSTREE_REPO_FILE}"
fi

# If the image need to be imported as an AMI, then we need to upload a RAW uncompressed version
if [[ "${IMPORT_IMAGE}" =~ ^(True|yes)$ ]]; then
    echo "[+] Uncompressing image ${IMAGE_FILE}"
    IMAGE_FILE=$(uncompress_image "${DOWNLOAD_DIRECTORY}" "${IMAGE_FILE}")
    create_checksum_file "${DOWNLOAD_DIRECTORY}" "${IMAGE_FILE}"
    echo "[+] Uploading uncompressed image ${IMAGE_FILE} and its checksum to 's3://${s3_path}'"
    upload_image_and_checksum_to_s3 "${DOWNLOAD_DIRECTORY}/${IMAGE_FILE}" "${s3_upload_target}/${IMAGE_FILE}"
fi

# Import logs and config files of the image
if [ -d "${TMT_PLAN_DATA}" ]; then
    LOGS_DIR="${IMAGE_FILE%%${UUID}*}${UUID}.logs"
    VARS_FILE="${TMT_PLAN_DATA}/var.yml"
    if [[ "${STREAM}" == "upstream" ]]; then
      RELEASE_DIR="${PRODUCT_BUILD_PREFIX}-${DATE}"
    else
      RELEASE_DIR="${RELEASE_ID}"
    fi
    # Update distro_baseurl field in vars.yml
    sed -i -E "s|(distro_baseurl: ).*repos|\1${WEBSERVER_RELEASES}/${X_STREAM}/${RELEASE_DIR}/repos|" "$VARS_FILE"
    COMPRESSED_LOGS=$(compress_build_logs "${TMT_PLAN_DATA}" "${LOGS_DIR}")
    echo "[+] Uploading compressed logs ${COMPRESSED_LOGS} to 's3://${s3_path}'"
    s3_cp "${COMPRESSED_LOGS}" "${s3_upload_target}/${COMPRESSED_LOGS}"
fi

# Only import images used for testing in the pipeline
if do_not_import_image ; then
  echo "[+] The image is not for testing, it won't be imported as AMI"
  exit 0
fi

# SHARING AMI
if [[ "${IMAGE_NAME}" == "cki" ]]; then
  aws_target_region="${AWS_CKI_REGION}"
  aws_target_account_id="${AWS_CKI_ACCOUNT_ID}"
elif [[ "${BUILD_TARGET}" == "aws" ]]; then
  aws_target_region="${AWS_TF_REGION}"
  aws_target_account_id="all"
else
  aws_target_region="${AWS_TF_REGION}"
  aws_target_account_id="${AWS_TF_ACCOUNT_ID}"
fi

echo "[+] Check if AMI already exists"
registered_ami_id=$(ami_registered_from_snapshot "$IMAGE_KEY" "$aws_target_region")

if [[ "$registered_ami_id" == *"ami-"* ]]; then
  echo "AMI already exists...registraion is not required"
  IMAGE_ID="${registered_ami_id}"
  echo "Image ID: ${IMAGE_ID}"
  exit 0
fi
echo "[+] Import snapshot to EC2"

S3_KEY="${S3_UPLOAD_PREFIX}/${IMAGE_FILE}"
IMPORT_SNAPSHOT_ID=$(import_snapshot "${S3_BUCKET_NAME}" "${S3_KEY}")

echo "[+] Waiting for snapshot $IMPORT_SNAPSHOT_ID import"
status=$(task_progress import_status "$IMPORT_SNAPSHOT_ID" "completed" "deleted")
echo "snapshot status: $status"

describe_import_snapshot_tasks "$IMPORT_SNAPSHOT_ID"
SNAPSHOT_ID=$(snapshot_id "$IMPORT_SNAPSHOT_ID")

echo "[+] Removing RAW images and json files from S3 bucket"
delete_raw_images "${S3_BUCKET_NAME}" "${S3_UPLOAD_PREFIX}" "${IMAGE_KEY}"

echo "[+] Copy snapshot from ${AWS_REGION} to ${aws_target_region}"
SNAPSHOT_ID=$(copy_snapshot "$SNAPSHOT_ID" "${AWS_REGION}" "${aws_target_region}")

echo "[+] Waiting for copy to complete"
status=""
status=$(task_progress snapshot_state "$SNAPSHOT_ID:${aws_target_region}" "completed")
echo "snapshot status: $status"
create_snapshot_tags "$SNAPSHOT_ID" "${aws_target_region}"

echo "[+] Register AMI from snapshot"
arch=$(fix_arch_name "${ARCH}")
root_device="/dev/sda1"
boot_mode="uefi"
IMAGE_ID=$(register_image "${IMAGE_KEY}" \
                          "${aws_target_region}" \
                          "${arch}" \
                          "${root_device}" \
                          "${boot_mode}" \
                          "$SNAPSHOT_ID" \
          )

create_image_tags "$IMAGE_ID" \
                  "${aws_target_region}" \
                  "${UUID}"

# Wait for image registration
echo "[+] Waiting for image $IMAGE_ID registration"
status=$(task_progress image_register_status "${IMAGE_ID}:${aws_target_region}" "available")
echo "image registration status: $status"

# Give permissions to the Testing Farm provisioner account
echo "[+] Granting permissions for $IMAGE_ID to the Testing Farm provisioner"
grant_image_permissions "${IMAGE_ID}" "${aws_target_region}" "${aws_target_account_id}"

echo "Image info:"
image_register_info "$IMAGE_ID" "${aws_target_region}"
