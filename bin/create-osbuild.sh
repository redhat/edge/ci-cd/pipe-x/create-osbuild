#!/bin/bash

set -euxo pipefail

source lib/create-osbuild.sh
source lib/build-env.sh

cd "${SAMPLE_IMAGES}" || exit 1

set_vars

install_dependencies

echo "[+] Prepare the dev environment"
prepare_env

echo "[+] Set the automotive-image-build parameters"
set_osbuild_options

echo "[+] Build the image ${DISK_IMAGE}"
build_target "${DISK_IMAGE}"

echo "[+] Moving the generated image"
# In the case of .aboot.simg, the IMAGE_FILE is a directory and the sparse
# image (rootfs.simg) is inside.
# So, we use the directory name, which doesn't have the extension .simg.
if [[ "${BUILD_FORMAT}" == "aboot.simg" ]]; then
  IMAGE_FILE="${IMAGE_FILE%.simg}"
fi

# Move and rename the image file
mkdir -p "$(dirname "$IMAGE_FILE")"
mv "$DISK_IMAGE" "$IMAGE_FILE"

# Move and rename the ostree-repo for the image, if it exists
if [[ -d "${OSTREE_REPO_DIR}" ]]; then
  mv "${OSTREE_REPO_DIR}" "$IMAGE_OSTREE_REPO"
fi

echo "[+] Saving some files for possible debugging at ${TMT_PLAN_DATA}"
save_files

echo "The final image is here: ${IMAGE_FILE}"
echo "Checksum for the image is in ${IMAGE_FILE%.*}.sha256"
