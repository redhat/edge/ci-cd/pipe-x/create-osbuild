#!/bin/bash

set -eux

source lib/build-env.sh

LOCAL_CONTAINER_IMAGE="localhost/builder:latest"
STREAM="${STREAM:-upstream}"
NIGHTLY_COMPOSE="${NIGHTLY_COMPOSE:-https://autosd.sig.centos.org/AutoSD-9/nightly}"
# Use a passed BASE_URL (for releases) of the nightly compose
BASE_URL="${BASE_URL:-${NIGHTLY_COMPOSE}}"
BUILDER_CONTEXT="builder"
CONTAINER_REGISTRY_TAG="${UUID:-latest}"

install_dependencies

build_container_image "${BUILDER_CONTEXT}" "${STREAM}" "${BASE_URL}" "${LOCAL_CONTAINER_IMAGE}"

publish_container_image "${STREAM}" \
                        "${LOCAL_CONTAINER_IMAGE}" \
                        "${CONTAINER_REGISTRY}" \
                        "${CONTAINER_REGISTRY_NAMESPACE}" \
                        "${CONTAINER_REGISTRY_TAG}" \
                        "${CONTAINER_REGISTRY_USER}" \
                        "${CONTAINER_REGISTRY_PASSWORD}"