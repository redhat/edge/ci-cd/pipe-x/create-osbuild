#!/bin/bash

source lib/create-osbuild.sh

set_vars

if [[ "${BUILD_FORMAT}" != "aboot.simg" ]]; then
  exit 0
fi

PARTITION_SIZE=64
KEEP_FREE_SPACE=5
MB=$((1024*1024))

max_file_size_m=$((PARTITION_SIZE - KEEP_FREE_SPACE))
max_file_size_b=$((max_file_size_m * MB))

file_path="${IMAGE_FILE%.simg}/aboot.img"
file_size=$(stat -c %s "${file_path}")

if [ "$file_size" -gt "${max_file_size_b}" ]; then
    echo "ERROR: 'aboot.img' size should be less then '${max_file_size_b}' bytes (${max_file_size_m}MB) but its actual size is: '${file_size}' bytes."
    exit 1
fi
