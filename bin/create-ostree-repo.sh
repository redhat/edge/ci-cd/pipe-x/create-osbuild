#!/bin/bash

set -ex

source lib/create-osbuild.sh
source lib/build-env.sh

cd "${SAMPLE_IMAGES}" || exit 1

set_vars

install_dependencies

echo "[+] Prepare the dev environment"
prepare_env

echo "[+] Set the automotive-image-build parameters"
set_osbuild_options

MANIFEST_DIR="osbuild-manifests"
ostree_repo="${MANIFEST_DIR}/${OSTREE_REPO_DIR}"
target_name="${BASE_TARGET}.repo"

repo_dir_name=$(download_and_unpack_ostree_repo "${BASE_URL}" "${IMAGE_KEY}")

# Make sure the direcory doesn't exists so the move doesn't fail
rm -fr "${ostree_repo}"
# Rename the OSTree repo directory to match $OSTREE_REPO
mv "${repo_dir_name}" "${ostree_repo}"

echo "[+] Build the image ${DISK_IMAGE}"
build_target "${target_name}"

# If it runs on tmt, we make sure save the OSTree repo
if [ -d "${TMT_PLAN_DATA}" ]; then
  mv "osbuild-manifests/${OSTREE_REPO_DIR}" "${TMT_PLAN_DATA}/"
fi

