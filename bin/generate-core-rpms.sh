#!/usr/bin/bash
set -euxo pipefail

AIB=${AIB:=automotive-image-builder/automotive-image-builder}

if [ "$#" != 4 ]; then
   echo "Missing args: distro arch yum_repo container_image"
   exit 1
fi

DISTRO=$1
ARCH=$2
YUM_REPO=$3
CONTAINER_IMAGE=$4

# Targets used to compute the core RPM set:
TARGETS="qemu"
if [ $ARCH == aarch64 ] ; then
    TARGETS="$TARGETS rcar_s4"
fi

MODES="image package"
OPTIONS="--define use_static_ip=true --define distro_baseurl=${YUM_REPO}"

run_aib() {
    if [[ "${USE_AIB_RPM}" == "True" ]]; then
        podman run -i -v "$PWD":/host -w /host --rm --privileged "$CONTAINER_IMAGE" /bin/bash -c "automotive-image-builder list-rpms --distro $DISTRO --target $TARGET --mode $MODE --arch $ARCH $OPTIONS images/qm-minimal.mpp.yml"
    else
        ${AIB} --include . --container --container-image-name "$CONTAINER_IMAGE" list-rpms --distro $DISTRO --target $TARGET --mode $MODE --arch $ARCH $OPTIONS images/qm-minimal.mpp.yml
    fi
}

extract_rpmlist() {
    jq -r '.rootfs | to_entries | .[] | .key + "\t(" + (.value.sourcerpm | rtrimstr(".src.rpm"))  + ")"'
}

for MODE in $MODES; do
    for TARGET in $TARGETS; do
        run_aib | extract_rpmlist
    done
done | sort | uniq
