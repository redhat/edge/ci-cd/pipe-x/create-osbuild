#!/bin/bash

install_git() {
  dnf install -y --setopt install_weak_deps=false git-core
}

check_ssl() {
  if [[ "${SSL_VERIFY}" == "false" ]]; then
    export GIT_SSL_NO_VERIFY=true
  fi
}

git_clone () {
  local repo_url="$1"
  local repo_dir="$2"
  # Repeat the clone until 5 times and waits 10 seconds in between if it is failed
  local n=0

  check_ssl

  until [[ $n -ge 5 ]]
  do
    git clone "${repo_url}" "${repo_dir}" && break || {
      ((n++))
      sleep 10
    }
  done
}

git_checkout() {
  local repo_dir="$1"
  local revision="$2"

  pushd "${repo_dir}" || exit 1

  if ! git checkout "${revision}" ; then
    echo "ERROR: ${revision} not found at the git repository" >&2
    return 1
  fi

  popd || exit 1
}

clone_repo() {
  local repo_url="$1"
  local repo_dir="$2"
  local revision="$3"

  git_clone "${repo_url}" "${repo_dir}"
  git_checkout "${repo_dir}" "${revision}"

}
