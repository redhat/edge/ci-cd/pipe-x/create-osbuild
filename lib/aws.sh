#!/bin/bash
# shellcheck disable=SC2153

install_dependencies() {
  dnf install -y jq python3-pip tree
  pip3 install awscli
}

configure_aws_settings() {
  local region="$1"

  aws configure set default.region "${region}"
  aws configure set default.output json
  # Provide some traceability for debugging
  # This avoids leaking secrets so should be safe
  aws configure list
  if [ -z "${AWS_ACCESS_KEY_ID}" ]; then
    echo "AWS_ACCESS_KEY_ID is empty."
  fi
  if [ -z "${AWS_SECRET_ACCESS_KEY}" ]; then
    echo "AWS_ACCESS_KEY_ID is empty."
  fi
}

is_release() {
  # If it's a nightly/latest build, it's not an official release
  # We need both because upstream uses nightly while downstream uses latest-*
  if [[ "${RELEASE}" =~ (nightly|latest-*) ]]; then
    return 1
  else
    return 0
  fi
}

to_keep() {
  # Should we keep the AMI or Snapshot?
  # Keep only for official releases
  if is_release ; then
    echo "True"
  else
    echo "False"
  fi
}

set_build_format() {
  # .raw file is needed for import as AMI in S3
  if [[ "${BUILD_FORMAT}" == "img" ]]; then
    echo "raw"
  elif [[ "${BUILD_FORMAT}" == "aboot.simg" ]]; then
    echo "aboot"
  else
    echo "${BUILD_FORMAT}"
  fi
}

set_s3_upload_prefix() {
  local ostree_repo="$1"

  if [[ "${ostree_repo}" == "yes" ]]; then
    echo "${UUID}/ostree-repos"
    return
  fi

  if [[ -n "${S3_UPLOAD_DIR}" ]]; then
    echo "${UUID}/${S3_UPLOAD_DIR}"
  elif [[ "${IMPORT_IMAGE}" =~ ^(True|yes)$ ]]; then
    echo "${UUID}/raw-images"
  elif [[ "${SAMPLE_IMAGE}" =~ ^(True|yes)$ ]]; then
    echo "${UUID}/sample-images"
  else
    echo "${UUID}/non-sample-images"
  fi
}

create_checksum_file() {
  local dir="$1"
  local image_file_name="$2"

  cd "${dir}" || exit 1
  sha256sum "${image_file_name}" > "${image_file_name}".sha256
}

compress_build_logs() {
  local build_logs_dir="$1"
  local build_logs_dir_name="$2"
  local compressed_tarball="${build_logs_dir_name}.tgz"

  tar -czf "${compressed_tarball}" -C "${build_logs_dir}" .
  echo "${compressed_tarball}"
}

package_ostree_repo() {
  local images_dir="$1"
  local repo_dir_name="$2"
  local tarball="${repo_dir_name}.tar"

  tar -cf "${images_dir}/${tarball}" "${repo_dir_name}"
  echo "${tarball}"
}

compress_image() {
  local images_dir="$1"
  local image_file_name="$2"

  # use all processors to compress and reduce job time and smaller dictionary to reduce memory requirement
  xz -v -2 -T0 "${images_dir}/${image_file_name}"
  echo "${image_file_name}.xz"           # add ".xz" to file extension for simplicity
}

uncompress_image() {
  local images_dir="$1"
  local image_file_name="$2"

  # use all processors to compress and reduce job time and smaller dictionary to reduce memory requirement
  unxz -v -2 -T0 "${images_dir}/${image_file_name}"
  echo "${image_file_name%.xz}"

}

upload_image_and_checksum_to_s3() {
  local src="$1"
  local target="$2"

  s3_cp "${src}" "${target}"
  s3_cp "${src}.sha256" "${target}.sha256"
}

s3_cp() {
  local VARIABLES_COUNT=$#
  if [ ${VARIABLES_COUNT} -eq 3 ] || [ ${VARIABLES_COUNT} -eq 2 ]; then
    local src="$1"
    local dest="$2"
    local aws_vars="$3"
  else
    echo -n "Expecting 2 or 3 variables defined but got ${VARIABLES_COUNT}. Exiting" >&2
    return 5
  fi

  aws s3 cp "${src}" "s3://${dest}" --only-show-errors${aws_vars:+ $aws_vars}
}

do_not_import_image() {
  if [[ "${IMPORT_IMAGE}" =~ ^(True|yes)$ ]]; then
    return 1
  else
    return 0
  fi
}

import_snapshot() {
  local VARIABLES_COUNT=$#
  if [ ${VARIABLES_COUNT} -eq 2 ]; then
    local s3bucket="$1"
    local s3key="$2"
  else
    echo -n "Expecting 2 variables defined but got ${VARIABLES_COUNT}. Exiting" >&2
    return 5
  fi

  aws ec2 import-snapshot \
          --disk-container \
          Format=raw,UserBucket="{S3Bucket=${s3bucket},S3Key=${s3key}}" | \
  jq -r .ImportTaskId
}

describe_import_snapshot_tasks() {
  local import_task_id="$1"

  aws ec2 describe-import-snapshot-tasks --import-task-ids "${import_task_id}"
}

import_status() {
  local import_task_id="$1"

  describe_import_snapshot_tasks "${import_task_id}" | \
  jq -r .ImportSnapshotTasks[0].SnapshotTaskDetail.Status
}

snapshot_id() {
  local import_task_id="$1"

  describe_import_snapshot_tasks "${import_task_id}" | \
  jq -r .ImportSnapshotTasks[0].SnapshotTaskDetail.SnapshotId
}

create_snapshot_tags() {
  local VARIABLES_COUNT=$#
  if [ "${VARIABLES_COUNT}" -lt 2 ]; then
    echo -n "Expecting 2 variable defined but got ${VARIABLES_COUNT}. Exiting" >&2
    return 5
  else
    local snapshot_id="$1"
    local region="$2"
    local component="${3:-Automotive}"
    local owner="${4:-A-TEAM}"
    local environment="${5:-Prod}"
    local fedora_group="${6:-ci}"
    local keep="False"
  fi

  # Tag the Snapshot for keeping if the image is for a release
  keep=$(to_keep)

  aws ec2 create-tags \
          --resources "${snapshot_id}" \
          --region "${region}" \
          --tags Key=ServiceComponent,Value="${component}" \
                 Key=ServiceOwner,Value="${owner}" \
                 Key=ServicePhase,Value="${environment}" \
                 Key=Keep,Value="${keep}" \
                 Key=ReleaseName,Value="${RELEASE}" \
                 Key=FedoraGroup,Value="${fedora_group}" \
                 Key=Nightly,Value="${NIGHTLY}"
}

copy_snapshot() {
  local VARIABLES_COUNT=$#
  if [ "${VARIABLES_COUNT}" -eq 3 ]; then
    local snapshot_id="$1"
    local src_region="$2"
    local dest_region="$3"
  else
    echo -n "Expecting 3 variables defined but got ${VARIABLES_COUNT}. Exiting" >&2
    return 5
  fi

  aws ec2 copy-snapshot \
          --source-snapshot-id "${snapshot_id}" \
          --source-region "${src_region}" \
          --region="${dest_region}" | \
  jq -r .SnapshotId
}

snapshot_state() {
  if [[ "$1" =~ .*":".* ]]; then
    local snapshot_id="${1%%:*}" # string before the :
    local region="${1##*:}"   # string after the :
  else
    echo -n "Invalid argument, please use <snapshot_id>:<region> formant. Exiting" >&2
    return 5
  fi

  aws ec2 describe-snapshots \
          --region "${region}" \
          --snapshot-ids "${snapshot_id}" | \
  jq -r .Snapshots[0].State
}

fix_arch_name() {
  local arch="$1"

  # AWS need the aarch64 architecture to be arm64
  if [[ "${arch}" == "aarch64" ]]; then
    echo "arm64"
  else
    echo "${arch}"
  fi
}

register_image() {
  local VARIABLES_COUNT=$#
  if [ "${VARIABLES_COUNT}" -eq 6 ]; then
    local image_name="$1"
    local region="$2"
    local arch="$3"
    local root_device="$4"
    local boot_mode="$5"
    local snapshot_id="$6"
  else
    echo -n "Expecting 6 variables defined but got ${VARIABLES_COUNT}. Exiting" >&2
    return 5
  fi

  aws ec2 register-image \
          --name "${image_name}" \
          --region "${region}" \
          --architecture "${arch}" \
          --virtualization-type hvm \
          --root-device-name "${root_device}" \
          --ena-support \
          --boot-mode "${boot_mode}" \
          --block-device-mappings "[
   {
       \"DeviceName\": \"/dev/sda1\",
       \"Ebs\": {
           \"SnapshotId\": \"${snapshot_id}\"
       }
   }]" | jq -r .ImageId
}

create_image_tags() {
  local VARIABLES_COUNT=$#
  if [ "${VARIABLES_COUNT}" -lt 3 ]; then
    echo -n "Expecting at least 3 variables defined but got ${VARIABLES_COUNT}. Exiting" >&2
    return 5
  else
    local image_id="$1"
    local region="$2"
    local uuid="$3"
    local component="${4:-Artemis}"
    local service="${5:-Artemis}"
    local app_code="${6:-ARR-001}"
    local owner="${7:-TFT}"
    local environment="${8:-Prod}"
    local keep="False"
  fi

  # Tag the AMI for keeping if the image is for a release
  keep=$(to_keep)

  aws ec2 create-tags \
          --resources "$image_id" \
          --region="${region}" \
          --tags Key=ServiceComponent,Value="${component}" \
                 Key=ServiceName,Value="${service}" \
                 Key=AppCode,Value="${app_code}" \
                 Key=ServiceOwner,Value="${owner}" \
                 Key=ServicePhase,Value="${environment}" \
                 Key=Keep,Value="${keep}" \
                 Key=ReleaseName,Value="${RELEASE}" \
                 Key=PIPELINE_RUN_ID,Value="${uuid}" \
                 Key=Nightly,Value="${NIGHTLY}"
}

image_register_info() {
  local VARIABLES_COUNT=$#
  if [ "${VARIABLES_COUNT}" -eq 2 ]; then
    local image_id="$1"
    local region="$2"
  else
    echo -n "Expecting 2 variables defined but got ${VARIABLES_COUNT}. Exiting" >&2
    return 5
  fi

  aws ec2 describe-images \
          --image-ids "${image_id}" \
          --region "${region}"
}

image_register_status() {
  if [[ "$1" =~ .*":".* ]]; then
    local image_id="${1%%:*}" # string before the :
    local region="${1##*:}"   # string after the :
  else
    echo -n "Invalid argument, please use <image_id>:<region> formant. Exiting" >&2
    return 5
  fi

  image_register_info "$image_id" "${region}" | jq -r .Images[0].State
}

ami_registered_from_snapshot() {
  local VARIABLES_COUNT=$#
  if [ "${VARIABLES_COUNT}" -eq 2 ]; then
    local image_name="$1"
    local region="$2"
  else
    echo -n "Expecting 2 variables defined but got ${VARIABLES_COUNT}. Exiting" >&2
    return 5
  fi

  aws ec2 describe-images \
          --filters "Name=name,Values=${image_name}" \
          --region "${region}" | jq -r .Images[0].ImageId
}

find_oldest_public_image() {
  local region="$1"
  local arch
  local image_id

  arch="$(fix_arch_name "${ARCH}")"
  image_id=$(aws ec2 describe-images \
                     --filters "Name=architecture,Values=${arch}" "Name=is-public,Values=true" "Name=name,Values=auto-osbuild-*" \
                     --owner self \
                     --region "${region}" \
                     --query 'reverse(sort_by(Images,&CreationDate))[:1].ImageId' \
                     --output text)
  echo "${image_id}"
}

deregister_image() {
  local image_id="$1"
  local region="$2"

  if [[ -n "${image_id}" ]]; then
    aws ec2 deregister-image \
            --image-id "${image_id}" \
            --region "${region}"
  else
    echo "No public AMI found" >&2
  fi
}

grant_image_permissions() {
  local VARIABLES_COUNT=$#
  if [ "${VARIABLES_COUNT}" -eq 3 ]; then
    local image_id="$1"
    local region="$2"
    local user_id="$3"
  else
    echo -n "Expecting 3 variables defined but got ${VARIABLES_COUNT}. Exiting" >&2
    return 5
  fi

  if [[ "$user_id" == "all" ]]; then
    local users="Group=all"  # Public
    # Find and de-register the oldest public AMI to avoid reaching the limit (5)
    local old_image_id
    old_image_id=$(find_oldest_public_image "${region}")
    deregister_image "${old_image_id}" "${region}"
  else
    local users="UserId=${user_id}"  # Private to that account
  fi

  aws ec2 modify-image-attribute \
          --image-id "${image_id}" \
          --region "${region}" \
          --launch-permission "Add=[{${users}}]"
}

task_progress() {
  local VARIABLES_COUNT=$#
  if [ "$VARIABLES_COUNT" -eq 3 ] || [ "${VARIABLES_COUNT}" -eq 4 ]; then
    local check_function="$1"
    local item_id="$2"
    local stop_state="$3"
    local error_state="$4"
  else
    echo -n "Expecting 3 or 4 variables defined but got ${VARIABLES_COUNT}. Exiting" >&2
    return 5
  fi

  set +x # Disable the debug during the loop
  until [[ "$state" == "$stop_state" ]]; do
    state=$($check_function "${item_id}")
    if [[ "$state" == "$error_state" ]]; then
      echo "ERROR: state = $error_state"
      exit 1
    fi
    # Send progress dots to stderr to leave clean the stdout for the func return
    echo -n "." >&2
  done
  set -x # Enable the debug again
  echo "$state"
}

get_compose() {
  local VARIABLES_COUNT=$#
  if [ "${VARIABLES_COUNT}" -eq 1 ]; then
    local compose_url="$1"
  else
    echo -n "Expecting at least 1 variables defined but got ${VARIABLES_COUNT}. Exiting" >&2
    return 5
  fi

  if [[ "${RELEASE_NAME}" == "nightly" ]]; then
    compose_id=$(curl "${compose_url}/metadata/composeinfo.json" | jq -r '.payload.compose.id')
    # Transform the nightly compose to a release compose.
    # Example:
    # From: http://internal.url.com/distro-9/nightly/DISTRO-9/latest-DISTRO-9/compose
    # To:   http://internal.url.com/distro-9/composes/DISTRO-9/DISTRO-9.3.0-20230220.0/compose
    echo "${compose_url/nightly/composes}" | \
    sed "s/latest-.*\(\/\)/${compose_id}\1/"
  else
    echo "${compose_url}"
  fi
}

delete_raw_images(){
  local VARIABLES_COUNT=$#
  if [ "${VARIABLES_COUNT}" -eq 3 ]; then
    local s3bucket="$1"
    local s3upload_prefix="$2"
    local image_key="$3"
  else
    echo -n "Expecting 3 variables defined but got ${VARIABLES_COUNT}. Exiting" >&2
    return 5
  fi

  aws s3 rm "s3://${s3bucket}/${s3upload_prefix}/${image_key}.raw"
  aws s3 rm "s3://${s3bucket}/${s3upload_prefix}/${image_key}.json"
  aws s3 rm "s3://${s3bucket}/${s3upload_prefix}/${image_key}.raw.sha256"
}
