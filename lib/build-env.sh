#!/bin/bash

install_dependencies() {
  dnf install -y --setopt install_weak_deps=false podman
}

registry_login() {
  local registry="$1"
  local user="$2"
  local password="$3"

  podman login -u="${user}" -p="${password}" "${registry}"
}

pull_container_image() {
  local stream="$1"
  local local_container_image="$2"
  local registry="$3"
  local namespace="$4"
  local user="$5"
  local password="$6"
  local container_repo="${registry}/${namespace}"

  if [[ "${stream}" == "upstream" ]]; then
    builder_image="${container_repo}/autosd-builder:latest"
  else
    registry_login "${registry}" "${user}" "${password}"
    builder_image="${container_repo}/rhivos-builder:latest"
  fi

  # Pull the latest container image
  podman pull "${builder_image}" >&2

  # Rename it to localhost/builder:latest to avoid passing the name between scripts
  podman tag "${builder_image}" "${local_container_image}" >&2

  echo "${local_container_image}"
}

build_container_image() {
  local context="$1"
  local stream="$2"
  local base_url="$3"
  local builder_image="$4"
  local build_args_file="${context}/argfile.conf"

  if [[ "${stream}" == "upstream" ]]; then
    echo "BASE_URL=${base_url}" > "${build_args_file}"
  else
    cat <<EOF > "${build_args_file}"
BASE_URL=${base_url}
DISTRO=RHIVOS
RELEASE_PKG=redhat-release
GPG_KEY=RPM-GPG-KEY-redhat-release
EOF
  fi

  # Build the local container image
  podman build \
         -f "${context}/Containerfile" \
         -t "${builder_image}" \
         --build-arg-file "${build_args_file}" \
         --label "quay.expires-after=5d" \
         "${context}/" >&2

  echo "${builder_image}"
}

publish_container_image() {
  local stream="$1"
  local local_container_image="$2"
  local registry="$3"
  local namespace="$4"
  local tag="$5"
  local user="$6"
  local password="$7"
  local container_repo="${registry}/${namespace}"
  local arch

  arch=$(arch)

  registry_login "${registry}" "${user}" "${password}"

  #Q: Should we pass a tag for MRs or releases?
  if [[ "${stream}" == "upstream" ]]; then
    builder_name="autosd-builder"
  else
    builder_name="rhivos-builder"
  fi

  builder_image="${container_repo}/${builder_name}:${tag}-${arch}"

  podman tag "${local_container_image}" "${builder_image}" >&2

  podman push "${builder_image}" "docker://${builder_image}" >&2
}

prepare_env() {
  if [[ "${USE_LATEST}" == "True" ]] ; then
    # Pull the latest container image and rename it as localhost/builder:latest
    pull_container_image "${STREAM}" \
                         "${LOCAL_CONTAINER_IMAGE}" \
                         "${CONTAINER_REGISTRY}" \
                         "${CONTAINER_REGISTRY_NAMESPACE}" \
                         "${CONTAINER_REGISTRY_USER}" \
                         "${CONTAINER_REGISTRY_PASSWORD}"
  else
    # Build a local localhost/builder:latest image using
    # the pipeline's compose as source for the packages
    build_container_image "${BUILDER_CONTEXT}" "${STREAM}" "${BASE_URL}" "${LOCAL_CONTAINER_IMAGE}"
  fi
}