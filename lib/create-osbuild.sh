#!/bin/bash

# Get OS data.
source /etc/os-release

# Set some absolute paths for later usage
if [[ -d "${TMT_TREE}" ]]; then
  export WORKDIR="${TMT_TREE}"
else
  export WORKDIR="$PWD"
fi
export SAMPLE_IMAGES="${WORKDIR}/sample-images"

_get_image_manifest() {
  local custom_images_dir="$1"
  local disk_image="$2"
  local script="${SAMPLE_IMAGES}/tools/image-from-filename"

  # Image manifest location, for the AIB
  if [ -x "${script}" ]; then
    ${script} "$custom_images_dir" "$disk_image"
  else
    echo "ERROR: ${script} not found"
    exit 1
  fi
}

set_vars() {
  AIB="${AIB:-${SAMPLE_IMAGES}/automotive-image-builder/automotive-image-builder}"
  DISTRO_VERSION=${DISTRO_VERSION:-1}
  DISTRO="${PACKAGE_SET}"
  # DISK_IMAGE is the target for the Makefile and the name of the resulted file.
  # Example: cs9-qemu-minimal-ostree.aarch64.img
  BASE_TARGET="${DISTRO}-${BUILD_TARGET}-${IMAGE_NAME}-${IMAGE_TYPE}.${ARCH}"
  export DISK_IMAGE="${BASE_TARGET}.${BUILD_FORMAT}"
  # .raw file is needed for import as AMI in S3
  if [[ "${BUILD_FORMAT}" == "img" ]]; then
    BUILD_FORMAT="raw"
  fi
  # Convert to the new image mode
  if [[ "${IMAGE_TYPE}" == "ostree" ]]; then
    IMAGE_MODE="image"
  else
    IMAGE_MODE="package"
  fi
  BASE_URL=${BASE_URL:-${CI_REPOS_ENDPOINT}/${UUID}}
  IMAGE_FILE=${IMAGE_FILE:-"${DOWNLOAD_DIRECTORY}/${IMAGE_KEY}.${BUILD_FORMAT}"}
  IMAGE_OSTREE_REPO=${IMAGE_OSTREE_REPO:-"${DOWNLOAD_DIRECTORY}/${IMAGE_KEY}.repo"}
  STREAM=${STREAM:-"upstream"}
  EXTEND_DEFINES=()
  PACKAGE_FOR_UPDATE="tmux"
  CUSTOM_IMAGES_DIR="custom-images"
  # Image manifest location, for the AIB
  IMAGE_MANIFEST=$(_get_image_manifest "$CUSTOM_IMAGES_DIR" "$DISK_IMAGE")
  DEFINES_VAR_FILE="var.yml"
  OSBUILD_VERSION="${OSBUILD_VERSION:-}"
  IMAGE_SIZE="${IMAGE_SIZE:-"4294967296"}"  # 4294967296 = 4 Gb
  OSTREE_OS_NAME="${OSTREE_OS_NAME:-centos}"
  UEFI_VENDOR="${UEFI_VENDOR:-centos}"
  USE_PRIVATE_REGISTRY="${USE_PRIVATE_REGISTRY:-false}"
  OSTREE_REPO="${OSTREE_REPO:-false}"
  OSTREE_REPO_DIR="${OSTREE_REPO_DIR:-ostree-repo}"
  UPDATE_OSTREE_REPO="${UPDATE_OSTREE_REPO:-false}"
  SET_IMAGE_SIZE="${SET_IMAGE_SIZE:-false}"
  ENABLE_FUSA="${ENABLE_FUSA:-false}"
  BUILD_CMD=""
  GENERATE_CORE_RPMS="${WORKDIR}/bin/generate-core-rpms.sh"
  LOCAL_CONTAINER_IMAGE="localhost/builder:latest"
  # shellcheck disable=SC2034
  BUILDER_CONTEXT="${WORKDIR}/builder"
  USE_LATEST="${USE_LATEST:-False}"
  export USE_AIB_RPM="${USE_AIB_RPM:-False}"
  CONTAINER_REGISTRY="${CONTAINER_REGISTRY:-quay.io}"
  CONTAINER_REGISTRY_NAMESPACE="${CONTAINER_REGISTRY_NAMESPACE:-automotive-toolchain}"
  CONTAINER_REGISTRY_USER="${CONTAINER_REGISTRY_USER:-}"
  CONTAINER_REGISTRY_PASSWORD="${CONTAINER_REGISTRY_PASSWORD:-}"
}

# Add options to the osbuild to enable the SSH access to the image
enable_ssh() {
  OS_OPTIONS+=("ssh_permit_root_login: true")
  if [[ "${ENABLE_SSH_PASSWORD_AUTH:-}" == "true" ]]; then
    OS_OPTIONS+=("ssh_permit_password_auth: true")
  else
    OS_OPTIONS+=("ssh_permit_password_auth: false")
  fi
  OS_OPTIONS+=("extra_rpms: [openssh-server,python3,polkit,rsync]")
}

disable_module_sig_enforce() {
  OS_OPTIONS+=("use_module_sig_enforce: false")
}

add_ssh_key() {
  local ssh_public_key="${1}"
  # new syntax introduced in https://gitlab.com/CentOS/automotive/src/automotive-image-builder/-/merge_requests/222
  OS_OPTIONS+=("root_ssh_key: '${ssh_public_key}'" "root_ssh_keys: ['${ssh_public_key}']")
}

add_ca_cert() {
  OS_OPTIONS+=("add_ca_cert: true")
}

# and to enable image size specification, with a 4GB default size
set_image_size(){
  local image_size="${1}"
  # Add the extra quotes to be sure the size is treated as a string
  OS_OPTIONS+=("image_size: '${image_size}'")
}

# Use the yum repo from the pipeline
override_yum_repo_with_product_build () {
  local base_url="${1}"
  local compose_distro="${2}"
  local yum_repo="${base_url}/repos/${compose_distro}/compose"

  OS_OPTIONS+=("distro_baseurl: ${yum_repo}")
  echo "${yum_repo}"
}

override_kernel_rpm() {
  local kernel_rpm="${1}"

  OS_OPTIONS+=("kernel_package: ${kernel_rpm}")
}

override_firmware_rpm() {
  local firmware_rpm="${1}"

  OS_OPTIONS+=("linux_firmware_rpm: ${firmware_rpm}")
}

override_release_rpm() {
  local kernel_rpm="${1}"

  OS_OPTIONS+=("release_rpm: ${release_rpm}")
}

add_aboot_kernel_opts() {
  if [[ "${BUILD_TARGET}" == "qdrive3" ]]; then
    partlabel="userdata"
    # qdrive3 aboot must fit into 50MB, so using gzip compression for initrd in aboot image
    # size has increased since ER4
    OS_OPTIONS+=("use_debug: true")
  else
    partlabel="system_a"
  fi
  OS_OPTIONS+=("extra_kernel_opts: ['root=PARTLABEL=${partlabel}','crashkernel=512M']")
}

add_build_info() {
  OS_OPTIONS+=("image_uuid: ${UUID}")
  OS_OPTIONS+=("release_name: ${RELEASE}")
}

add_uefi_vendor() {
  OS_OPTIONS+=("uefi_vendor: ${UEFI_VENDOR}")
}

add_ostree_os_name() {
  OS_OPTIONS+=("osname: ${OSTREE_OS_NAME}")
}

add_diff_for_update_test(){
  EXTEND_DEFINES=(
    distro_version="${DISTRO_VERSION}.1"
    extra_rpms=["${PACKAGE_FOR_UPDATE}"]
  )
}

set_osbuild_options() {

  add_uefi_vendor

  if [[ "${IMAGE_TYPE}" == "ostree" ]]; then
    add_ostree_os_name
  fi

  if [[ "${UPDATE_OSTREE_REPO}" == "yes" ]]; then
    add_diff_for_update_test
  fi

  if [[ "${TEST_IMAGE}" == "yes" ]]; then
    enable_ssh
    if [[ -n "${SSH_KEY:-}" ]]; then
      add_ssh_key "${SSH_KEY}"
    fi
  fi

  override_yum_repo_with_product_build "${BASE_URL}" "${COMPOSE_DISTRO}"

  if [[ "${USE_MODULE_SIG_ENFORCE:-}" == "false" ]]; then
    disable_module_sig_enforce
  fi

  if [[ -n "${KERNEL_RPM:-}" ]]; then
    override_kernel_rpm "${KERNEL_RPM}"
  fi

  if [[ -n "${FIRMWARE_RPM:-}" ]]; then
    override_firmware_rpm "${FIRMWARE_RPM}"
  fi

  if [[ -n "${RELEASE_RPM:-}" ]]; then
    override_release_rpm "${RELEASE_RPM}"
  fi

  if [[ "${BUILD_FORMAT}" == "aboot" ]]; then
    add_aboot_kernel_opts
  fi

  if [[ "${IMAGE_NAME}" == "qa" ]] && [[ "${OSTREE_REPO}" == "yes" ]] && [[ "${STREAM}" == "downstream" ]]; then
    add_ca_cert
  fi

  add_build_info

  if [[ "${SET_IMAGE_SIZE}" =~ ^(True|yes)$ ]]; then
    set_image_size "${IMAGE_SIZE}"
  fi
}

check_remote_file() {
  local filename="$1"

  curl --output /dev/null --silent --head --fail "${filename}"
}

download_and_unpack_ostree_repo(){
  local base_url="${1}"
  local repo_dir_name="${2}.repo"
  local tarball="${repo_dir_name}.tar"
  local tarball_url="${base_url}/ostree-repos/${tarball}"

  if ! check_remote_file "${tarball_url}" ; then
    echo "ERROR: the URL '${tarball_url}' doesn't exist"
    exit 1
  fi

  # Download the tarball
  curl --remote-name "${tarball_url}"

  if [[ ! -f "${tarball}" ]]; then
    echo "ERROR: file '${tarball}' not found"
    exit 1
  fi

  tar -xf "${tarball}"
  echo "${repo_dir_name}"
}

_save_config_files(){
  local arguments="$*"

  if [ -d "${TMT_PLAN_DATA}" ]; then
    # Save the build line for the logs
    echo "${arguments}" > "${TMT_PLAN_DATA}/build_command.log"
    # Save the defines var file
    if [[ -f "${DEFINES_VAR_FILE}" ]]; then
      cp -fv "${DEFINES_VAR_FILE}" "${TMT_PLAN_DATA}/"
    fi
  fi
}

run_container() {
  local BUILDDIR=_build
  local EXEC="cd /host; mkdir -p $BUILDDIR; cp -f /usr/bin/osbuild $BUILDDIR/osbuild; chcon system_u:object_r:install_exec_t:s0 $BUILDDIR/osbuild; export PATH=$BUILDDIR:\$PATH; export OSBUILD_BUILDDIR=$BUILDDIR; automotive-image-builder ${1:-}"
  podman run -i -v /dev:/dev -v "$PWD":/host -w /host --rm --privileged $LOCAL_CONTAINER_IMAGE /bin/bash -c "$EXEC"
}

build_target() {
  local target="${1}"
  local flags=()
  local enable_fusa=${ENABLE_FUSA}
  local defines_file="${DEFINES_VAR_FILE}"
  local extended_defines=()
  local export_format="${BUILD_FORMAT}"
  local container_image_name="${LOCAL_CONTAINER_IMAGE}"
  local container_flags=()

  # Rename some unsupported extensions to the equivalent at the a-i-b
  case "${BUILD_FORMAT}" in
    "raw")
        export_format="image"
        ;;
    "repo")
        export_format="ostree-commit"
        ;;
  esac

  flags=(
    --distro "${DISTRO}"
    --target "${BUILD_TARGET}"
    --mode "${IMAGE_MODE}"
    --arch "${ARCH}"
    --export "${export_format}"
    --osbuild-manifest "${BASE_TARGET}.json"
  )

  if [[ "${enable_fusa}" == "true" ]]; then
    flags+=(--fusa)
  fi

  container_flags=(
    --container
    --container-image-name "${container_image_name}"
  )

  # Create a file with all the defines to pass
  # But first, make sure to remove any previous data
  rm -f "${defines_file}"
  for option in "${OS_OPTIONS[@]}"; do
    echo "${option}" >> "${defines_file}"
  done
  echo "Defines file content:"
  cat "${defines_file}"
  echo

  for define in "${EXTEND_DEFINES[@]}"; do
    extended_defines+=(--extend-define "${define}")
  done

  if [[ "${USE_AIB_RPM}" == "True" ]]; then
    BUILD_CMD="--include . --verbose build ${flags[*]} --define-file ${defines_file}"
  else
    BUILD_CMD="--include . ${container_flags[*]} --verbose build ${flags[*]} --define-file ${defines_file}"
  fi
  if [[ -n "${EXTEND_DEFINES[*]}" ]]; then
    BUILD_CMD+=" ${extended_defines[*]}"
  fi
  if [[ "${OSTREE_REPO}" == "yes" ]]; then
    BUILD_CMD+=" --ostree-repo ${OSTREE_REPO_DIR}"
  fi

   # Add the image manifest and output file name
   BUILD_CMD+="  $IMAGE_MANIFEST ${target}"

  _save_config_files "$(basename "$AIB") ${BUILD_CMD}"

  # Start workaround: set SELinux permissive at the how to avoid SELinux from OSBuil inside the container
  setenforce 0

  # Run the make command (inside the container)
  echo "Running: automotive-image-builder ${BUILD_CMD[*]}"
  # shellcheck disable=SC2068
  if [[ "${USE_AIB_RPM}" == "True" ]]; then
    run_container "${BUILD_CMD[@]}"
  else
    ${AIB} ${BUILD_CMD[@]}
  fi

  # End workaround: set back SELinux to enfoce mode
  setenforce 1
}

create_core_rpms() {
  local distro="${1}"
  local arch="${2}"

  yum_repo=$(override_yum_repo_with_product_build "${BASE_URL}" "${COMPOSE_DISTRO}")

  BUILD_CMD=("${GENERATE_CORE_RPMS}" "${distro}" "${arch}" "${yum_repo}" "${LOCAL_CONTAINER_IMAGE}")

  _save_config_files "${BUILD_CMD[*]}"

  # shellcheck disable=SC2068
  ${BUILD_CMD[@]} > "${distro}-core-rpms-${arch}.txt"
}

# Save some file to the tmt/TF artifacts for debugging
save_files() {
  if [ -d "${TMT_PLAN_DATA}" ]; then
    # Save the osbuild manifest generated by osbuild-mpp
    # Format: cs9-aws-developer-regular.aarch64.json
    local manifest="${BASE_TARGET}.json"

    if [[ -f "${manifest}" ]] && [[ -d "${TMT_PLAN_DATA}" ]]; then
      cp -fv "${manifest}" "${TMT_PLAN_DATA}/"
    fi
  fi
}

